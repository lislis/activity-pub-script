# smoll activity pub thing (not a server)

This is a very hacky script as described in [Gargron's blog post](https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/) plus some more info on how to get it running.
Read the post first before attempting to use this.

## Running it

### You need

- webserver with a domain and valid with ssl certificate

- have openSSL installed

- Ruby 2.5.1 and bundler (`$ gem install bundler`)


### Setup

- Install ruby dependencies with `$ bundle install`

- Generate a keypair with

```bash

$ openssl genrsa -out private.pem 2048
$ openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```

- Think of a username and url where this should live

- Adjust `webfinger.json`, `actor.json` and `hello-world.json` with username and url for your user and server.

- Place the generated pubkey into `actor.json` as one line string (escape linebreaks with `\n`)!

- Adjust `hello-world.json` and `main.rb` with the username and host (+ post id) where you want to reply to.

- Adjust routes (eg via nginx)
  - `webfinger.json` must be returned when a webfinger request for the username comes in.
  - `actor.json` must be returned when a request comes in for the href specified in `webfinger.json`

- Run main.rb to send a toot

See the success here https://anticapitalist.party/@malte/100267922297749220
