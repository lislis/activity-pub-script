require 'http'
require 'openssl'

document      = File.read(ARGV[0])
date          = Time.now.utc.httpdate
keypair       = OpenSSL::PKey::RSA.new(File.read(ARGV[2]))
signed_string = "(request-target): post /inbox\nhost: #{ARGV[1]}\ndate: #{date}"
signature     = Base64.strict_encode64(keypair.sign(OpenSSL::Digest::SHA256.new, signed_string))
header        = 'keyId="https://enteignet.in-berlin.de/users/mother",headers="(request-target) host date",signature="' + signature + '"'

HTTP.headers({ 'Host': ARGV[1], 'Date': date, 'Signature': header })
  .post("https://#{ARGV[1]}/inbox", body: document)
